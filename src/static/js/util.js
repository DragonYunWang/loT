/**
 * @param {String} 手机号码
 * 验证手机号码
 */
function checkMobile(mobile) {
  var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/;
  if (mobile === '') {
    prompt('手机号码不能为空!');
    return false;
  }
  if (!(reg.test(mobile))) {
    prompt('手机号码格式不正确,请输入正确的手机号码!');
    return false;
  }
  return true;
}

/**
 * 解析url参数
 * @example ?id=12345&a=b
 * @return Object {id:12345,a:b}
 */
function urlParse() {
  var url = window.location.search;
  var obj = {};
  var reg = /[?&][^?&]+=[^?&]+/g
  var arr = url.match(reg)
  // ['?id=12345', '&a=b']
  if (arr) {
    arr.forEach(function (item) {
      var tempArr = item.substring(1).split('=');
      var key = decodeURIComponent(tempArr[0]);
      var val = decodeURIComponent(tempArr[1]);
      obj[key] = val;
    });
  }
  return obj;
}

/**
 * 时间格式化
 * @params {yyyy-MM-dd hh:mm:ss 或者 yyyy-MM-dd}
 */
function formatDate(date, fmt) {
  var o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      var str = o[k] + '';
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
    }
  }
  return fmt;
}

/**
 * 补零
 * @param {str}
 */
function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}

/**
 *
 * @param {string} content 提示信息
 * @param {skin 或者 footer}  type
 * @param {Date} time
 */
function prompt(content, type, time) {
  if (type == undefined) type = 'msg';
  if (time == undefined) time = 3;
  layer.open({
    content: content,
    skin: type,
    time: time // 3秒后自动关闭
  });
}
