$(function () {
  FastClick.attach(document.body);
  var isChecked;
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var ERR_OK = 0;
  var query = '?userId=' + userId + '&openId=' + openId + '&partnerId=' + partnerId;
  var rechargeNav = $('.recharge-nav');
  var queryParams = {
    method: 'queryUseIsCertificate',
    openId: openId,
    partnerId: partnerId,
    userId: userId
  }
  $.ajax({
    type: 'post',
    url: url,
    data: queryParams,
    async: false,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === ERR_OK) {
        var result = JSON.parse(res.resultMsg);
        if (result.ischeck === ERR_OK) {
          isChecked = 0;
        } else if (result.ischeck === 1) {
          isChecked = 1;
        } else {
          isChecked = 2;
        }
      } else {
        isChecked = 3
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });
  // 导航
  $('.nav li').on('click', function () {
    var _index = $(this).index();
    switch (_index) {
      case 0:
        window.location.href = 'views/realNameValidation.html' + query;
        break;
      case 1:
        switch (isChecked) {
          case 0:
            rechargeNav.slideToggle();
            break;
          case 1:
            prompt('实名审核中,请去实名认证通过后充值');
            break;
          case 2:
            prompt('审核未通过,请去实名认证通过后充值');
            break;
          case 3:
            prompt('未实名认证,请去实名认证通过后充值');
            break;
          default:
            break;
        }
        break;
      case 2:
        window.location.href = 'views/problem.html' + query;
        break;
      default:
        break;
    }
  });
  // 我要充值导航
  $('.recharge-nav li').on('click', function () {
    var _index = $(this).index();
    switch (_index) {
      case 0:
        window.location.href = 'views/recharge.html' + query;
        break;
      case 1:
        window.location.href = 'views/orderQuery.html' + query;
        break;
      case 2:
        window.location.href = 'views/search.html' + query;
        break;
      default:
        break;
    }
  });
});
