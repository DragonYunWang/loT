$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var ERR_OK = 0;
  var method = 'queryOrderInfo';
  var paramsData = {
    method: method,
    openId: openId,
    partnerId: partnerId,
    userId: userId
  }
  var orderListWrapper = $('.order-list-wrapper');
  // 定义过滤器
  template.defaults.imports.money = function (value) {
    return '¥' + value / 100;
  }

  template.defaults.imports.packageSize = function (value) {
    return value < 1024 ? value + 'MB' : value / 1024 + 'GB';
  }

  template.defaults.imports.rechargeStatus = function (value) {
    switch (value) {
      case 0:
        return '订购成功';
      case 1:
        return '充值中';
      case 2:
        return '失败';
      case 3:
        return '失败';
      default:
        break;
    }
  }

  template.defaults.imports.validityPeriod = function (value) {
    switch (value) {
      case 'm':
        return 1;
      case 'q':
        return 3;
      case 'h':
        return 6;
      case 'y':
        return 12;
      default:
        break;
    }
  }

  template.defaults.imports.timestamp = function (value) {
    return new Date(value)
  }

  template.defaults.imports.dateFormat = formatDate;

  template.defaults.imports.payType = function (value) {
    if (value === 2) {
      return '微信支付';
    }
  }

  // 查询
  $.ajax({
    type: 'POST',
    url: url,
    data: paramsData,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === ERR_OK) {
        var result = JSON.parse(res.resultMsg);
        var listStr = template('order-list', {
          list: result
        });
        orderListWrapper.html(listStr);
      } else {
        orderListWrapper.html('<p style="color: #000; width: 50%; margin: 50px auto;font-size: 0.5rem;">暂无订单记录 >></p>')
        prompt('暂无订单记录');
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });

  // 查看详情
  $('.order-list-wrapper').on('click', '.order-item .btn', function () {
    window.location.href = 'detail.html?' + $(this).data('detail');
  });
});
